<?php

namespace backend\module\form_layouts;

use Yii;
use yii\base\Module;
use yii\console\Application;

class FormLayoutsModule extends Module
{
    public $controllerNamespace = 'backend\module\form_layouts\controllers';

    public function init()
    {
        parent::init();
    }
}
