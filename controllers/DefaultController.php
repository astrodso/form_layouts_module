<?php

namespace backend\module\form_layouts\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\module\form_layouts\models\FormLayouts;
use backend\module\form_layouts\models\FormLayoutsSearchModel;
use backend\module\form_layouts\models\HtmlTemplates;
use backend\module\form_layouts\models\enum\FormLayoutsEntityTypeEnum;
use backend\module\form_layouts\helpers\TextHelper;
use common\models\BankTransferModel;
use backend\module\form_layouts\models\enum\HtmlTemplatesEnum;
use backend\module\form_layouts\models\HtmlTemplatesParts;
use common\models\BankClosingModel;

/**
 * DefaultController implements the CRUD actions for TaskModel model.
 */
class DefaultController extends Controller
{
    const EMPTY_STRING = 'empty';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST']
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'get-layout', 'get-form', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['accessAdminUsers'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    if (Yii::$app->user->isGuest) {
                        $this->redirect(['/site/login']);
                    } else {
                        throw new ForbiddenHttpException('You are not allowed to access this page');
                    }
                }
            ],
        ];
    }

    public function actionIndex()
    {

       $searchModel = new FormLayoutsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
  
    }

    /**
     * Creates a new TaskModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FormLayouts();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);

        } else {

            return $this->render('create', [
                'model' => $model
            ]);

        }

    }

    /**
     * Updates an existing TaskModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            return $this->redirect(['index']);

        } else {

            return $this->render('update', [
                'model' => $model
            ]);

        }

    }

    /**
     * Deletes an existing TaskModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetLayout()
    {
        $layoutType = Yii::$app->request->post('layoutType');

        $result = HtmlTemplates::findOne(['templateType' => $layoutType]);

        echo isset($result->body) ? $result->body : '';

    }

    public function actionGetForm()
    {
        $form = '';

        $formId = Yii::$app->request->post('formId');
        $entityType = Yii::$app->request->post('entityType');
        $entityId = Yii::$app->request->post('entityId');

        $result = FormLayouts::findOne(['id' => $formId]);
        
        if(isset($result->body)){

            $form = $this->getForm($entityType, $entityId, $result->body, $result->layoutType);
            
        }

        echo $form;

    }

    /**
     * Finds the TaskModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormLayouts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрошенной страницы не существует.');
        }
    }


    public function getForm($entityType, $entityId, $text, $layoutType)
    {
        $form = '';
        switch ($entityType) {
            case FormLayoutsEntityTypeEnum::BANK_TRANSFER:
                $model = BankTransferModel::find()->where(['id' => $entityId])->one();
                $form  = $this->getFormWithPlaceholdersBankTransfer($model, $text, $layoutType);
                break;
            case FormLayoutsEntityTypeEnum::BANK_CLOSING:
                $model = BankClosingModel::find()->where(['id' => $entityId])->one();
                $form  = $this->getFormWithPlaceholdersBankClosing($model, $text, $layoutType);
                break;
        }

        return $form;
    }

    public function getFormWithPlaceholdersBankTransfer(BankTransferModel $model, $text, $layoutType)
    {

        $form = '';
        $trs  = '';
        $i    = 0;
        $service_total_amount = 0;

        $tr_part_model = HtmlTemplatesParts::find()->where(['name' => 'invoice_payment_tr_part'])->one();

        $dealServicesModels = $model->initDealServices($model->deals);

        foreach ($dealServicesModels as $service) {

                $trs .= TextHelper::replacePlaceholder($tr_part_model->body, [
                    '{{service_number}}' => ++$i,
                    '{{service}}'        => $service[0]->content,
                    '{{service_count}}'  => $service[0]->quantity,
                    '{{service_unit}}'   => 'шт',
                    '{{service_price}}'  => $service[0]->price,
                    '{{service_summ}}'   => $service[0]->summary
                ]);

                $service_total_amount += $service[0]->summary;

        }

        switch ($layoutType) {
            case HtmlTemplatesEnum::INVOICE_PAYMENT:
                $form = TextHelper::replacePlaceholder($text, [
                    '{{organization}}'              => !empty($model->organization->name)
                        ? $model->organization->name : self::EMPTY_STRING,
                    '{{organization_address}}'      => !empty($model->organization->legalAddress)
                        ? $model->organization->legalAddress : self::EMPTY_STRING,
                    //TODO выбирать основной контакт
                    '{{office_phone}}'              => !empty($model->office->contacts[0]->phone)
                        ? $model->office->contacts[0]->phone : self::EMPTY_STRING,
                    '{{bank_organization}}'         => !empty($model->bankAccount->bank->name)
                        ? $model->bankAccount->bank->name : self::EMPTY_STRING,
                    '{{inn_organization}}'          => !empty($model->organization->inn)
                        ? $model->organization->inn : self::EMPTY_STRING,
                    '{{kpp_organization}}'          => !empty($model->organization->kpp)
                        ? $model->organization->kpp : self::EMPTY_STRING,
                    '{{bik_bank_organization}}'     => !empty($model->bankAccount->bank->bic)
                        ? $model->bankAccount->bank->bic : self::EMPTY_STRING,
                    '{{corr_bank_organization}}'    => !empty($model->bankAccount->bank->account_corr)
                        ? $model->bankAccount->bank->account_corr : self::EMPTY_STRING,
                    '{{account_number}}'            => !empty($model->bankAccount->account_number)
                        ? $model->bankAccount->account_number : self::EMPTY_STRING,
                    '{{bank_account_organization}}' => !empty($model->billNumber)
                        ? $model->billNumber : self::EMPTY_STRING,
                    '{{account_date}}'              => !empty($model->billDate)
                        ? $model->billDate : self::EMPTY_STRING,
                    '{{payer}}'                     => !empty($model->payer->name)
                        ? $model->payer->name : self::EMPTY_STRING,
                    '{{inn_payer}}'                 => !empty($model->payer->inn)
                        ? $model->payer->inn : self::EMPTY_STRING,
                    '{{kpp_payer}}'                 => !empty($model->payer->kpp)
                        ? $model->payer->kpp : self::EMPTY_STRING,
                    '{{payer_address}}'             => !empty($model->payer->legalAddress)
                        ? $model->payer->legalAddress : self::EMPTY_STRING,
                    '{{contract_name}}'             => !empty($model->contract->name)
                        ? $model->contract->name : self::EMPTY_STRING,
                    '{{tr_part}}'                   => $trs,
                    '{{service_total_summ}}'        => $service_total_amount,
                    '{{service_total_count}}'       => $i,
                    '{{service_total_summ_text}}'   => TextHelper::ucFirst( TextHelper::num2str($service_total_amount ) ),
                    //TODO выбирать с пометкой руководитель
                    '{{organization_director}}'     => !empty( $model->organization->responsibles[0]->name )
                        ? $model->organization->responsibles[0]->name : self::EMPTY_STRING,
                    //TODO выбирать с пометкой главбух
                    '{{organization_main_accountant}}'     => !empty( $model->organization->responsibles[0]->name )
                        ? $model->organization->responsibles[0]->name : self::EMPTY_STRING,
                ]);
                break;
        }

        return $this->removeTagTrPart($form);
    }

    public function getFormWithPlaceholdersBankClosing(BankClosingModel $model, $text, $layoutType)
    {

        $form = '';
        switch ($layoutType) {
            case HtmlTemplatesEnum::INVOICE:

                $tr_part = HtmlTemplatesParts::find()->where(['name' => 'invoice_tr_part'])->one();

                $form = TextHelper::replacePlaceholder($text,
                        [
                ]);
                break;
            case HtmlTemplatesEnum::ACT:

                $form = TextHelper::replacePlaceholder($text,
                        [
                ]);
                break;
        }

        return $form;
    }

    public function removeTagTrPart($form){
        return preg_replace('#<!--tr_part-->.*?<!--tr_part-->#s', '', $form);
    }

}
