<?php

namespace backend\module\form_layouts\migrations;

use yii\db\Migration;

/**
 * Class m171214_044211_topuzov_create_table_html_templates
 */
class m171214_044211_topuzov_create_table_html_templates extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('html_templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'body' => $this->text(),
            'templateType' => $this->integer(1)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
