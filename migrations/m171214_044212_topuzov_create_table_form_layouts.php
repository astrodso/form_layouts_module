<?php

namespace backend\module\form_layouts\migrations;

use yii\db\Migration;

/**
 * Class m171214_044212_topuzov_create_table_form_layouts
 */
class m171214_044212_topuzov_create_table_form_layouts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('form_layouts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'body' => $this->text(),
            'layoutType' => $this->integer(1),
            'entityType' => $this->string(50),
            'allowedVarList' => $this->string(255),
            'orientation' => $this->integer(1),
            'status' => $this->integer(1)->defaultValue(1),
            'createdAt' => $this->dateTime(),
            'updatedAt' => $this->dateTime(),
            'deletedAt' => $this->dateTime(),
            'createdBy' => $this->integer(),
            'updatedBy' => $this->integer(),
            'deletedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
