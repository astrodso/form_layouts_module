<?php

namespace backend\module\form_layouts\migrations;

use yii\db\Migration;

/**
 * Class m171218_044211_topuzov_create_table_html_templates_parts
 */
class m171218_044211_topuzov_create_table_html_templates_parts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('html_templates_parts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'body' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
