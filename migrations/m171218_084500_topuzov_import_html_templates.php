<?php

namespace backend\module\form_layouts\migrations;

use yii\db\Migration;
use yii\helpers\BaseFileHelper;

/**
 * Class m171218_084500_topuzov_import_html_templates
 */
class m171218_084500_topuzov_import_html_templates extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $module_dir = dirname(__DIR__, 1);

        $act             = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/act.tpl'));
        $invoice         = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/invoice.tpl'));
        $invoice_payment = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/invoice_payment.tpl'));

        $act_tr_part             = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/act_tr_part.tpl'));
        $invoice_tr_part         = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/invoice_tr_part.tpl'));
        $invoice_payment_tr_part = file_get_contents(BaseFileHelper::normalizePath($module_dir.'/templates/invoice_payment_tr_part.tpl'));

        $this->insert('html_templates',
            [
            'name'         => 'invoice_payment',
            'body'         => $invoice_payment,
            'templateType' => 3
        ]);

        $this->insert('html_templates',
            [
            'name'         => 'act',
            'body'         => $act,
            'templateType' => 2
        ]);

        $this->insert('html_templates',
            [
            'name'         => 'invoice',
            'body'         => $invoice,
            'templateType' => 1
        ]);


        //parts
        $this->insert('html_templates_parts',
            [
            'name' => 'invoice_payment_tr_part',
            'body' => $invoice_payment_tr_part
        ]);

        $this->insert('html_templates_parts',
            [
            'name' => 'act_tr_part',
            'body' => $act_tr_part
        ]);

        $this->insert('html_templates_parts',
            [
            'name' => 'invoice_tr_part',
            'body' => $invoice_tr_part
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}