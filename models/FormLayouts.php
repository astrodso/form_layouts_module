<?php

namespace backend\module\form_layouts\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "form_layouts".
 *
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property integer $layoutType
 * @property string $entityType
 * @property string $allowedVarList
 * @property integer $orientation
 * @property integer $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property integer $createdBy
 * @property integer $updatedBy
 * @property integer $deletedBy
 */
class FormLayouts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_layouts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['body'], 'string'],
                [['layoutType', 'orientation', 'status', 'createdBy', 'updatedBy',
                'deletedBy'], 'integer'],
                [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
                [['name', 'allowedVarList'], 'string', 'max' => 255],
                [['name'], 'required'],
                [['entityType'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'name'           => 'Название',
            'body'           => 'Макет',
            'layoutType'     => 'Тип макета',
            'entityType'     => 'Модуль использования',
            'allowedVarList' => 'Allowed Var List',
            'orientation'    => 'Альбомная ориентация',
            'status'         => 'Статус',
            'createdAt'      => 'Created At',
            'updatedAt'      => 'Updated At',
            'deletedAt'      => 'Deleted At',
            'createdBy'      => 'Created By',
            'updatedBy'      => 'Updated By',
            'deletedBy'      => 'Deleted By',
        ];
    }

}