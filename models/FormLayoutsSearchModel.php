<?php

namespace backend\module\form_layouts\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\module\form_layouts\models\FormLayouts;

/**
 * FormLayoutsSearchModel represents the model behind the search form about `backend\module\form_layouts\models\FormLayouts`.
 */
class FormLayoutsSearchModel extends FormLayouts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'layoutType', 'orientation', 'status', 'createdBy', 'updatedBy', 'deletedBy'], 'integer'],
            [['name', 'body', 'entityType', 'allowedVarList', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormLayouts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'layoutType' => $this->layoutType,
            'orientation' => $this->orientation,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'createdBy' => $this->createdBy,
            'updatedBy' => $this->updatedBy,
            'deletedBy' => $this->deletedBy,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'entityType', $this->entityType])
            ->andFilterWhere(['like', 'allowedVarList', $this->allowedVarList]);

        return $dataProvider;
    }
}
