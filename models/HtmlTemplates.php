<?php

namespace backend\module\form_layouts\models;

use Yii;

/**
 * This is the model class for table "html_templates".
 *
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property integer $templateType
 */
class HtmlTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'html_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['templateType'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'body' => 'Body',
            'templateType' => 'Template Type',
        ];
    }
}
