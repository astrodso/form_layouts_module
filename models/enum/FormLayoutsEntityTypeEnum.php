<?php

namespace backend\module\form_layouts\models\enum;

use common\models\enum\base\Enumerable;

class FormLayoutsEntityTypeEnum extends Enumerable
{
    const BANK_TRANSFER = 'bank-transfer';
    const BANK_CLOSING = 'bank-closing';

    public static $list = [
        self::BANK_TRANSFER => 'Приходы и расходы банк',
        self::BANK_CLOSING  => 'Закрывающие документы',
    ];

    public static function listData($exclude = array())
    {
        return self::$list;
    }
}