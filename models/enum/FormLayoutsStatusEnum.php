<?php
namespace backend\module\form_layouts\models\enum;

use common\models\enum\base\Enumerable;

class FormLayoutsStatusEnum extends Enumerable {

    const DELETED = 0;
    const ACTIVE = 1;

    public static $list = [
        self::ACTIVE => 'Активный',
        self::DELETED => 'Удаленный'
        
    ];

    public static function listData($exclude = array()) {
        return self::$list;
    }

}
