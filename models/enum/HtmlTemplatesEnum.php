<?php
namespace backend\module\form_layouts\models\enum;

use common\models\enum\base\Enumerable;

class HtmlTemplatesEnum extends Enumerable {

    const INVOICE = 1;
    const ACT = 2;
    const INVOICE_PAYMENT = 3;

    public static $list = [
        self::INVOICE => 'Счет-фактура',
        self::ACT => 'Акт',
        self::INVOICE_PAYMENT => 'Счет на оплату'
    ];

    public static function listData($exclude = array()) {
        return self::$list;
    }

}
