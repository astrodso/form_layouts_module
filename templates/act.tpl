<h1 style="border-bottom: 2px solid #000; width: 1000px; padding-bottom: 20px;"><strong>Акт {{act_number}} от {{act_date}} г.</strong></h1>
<table border="0" cellpadding="0" cellspacing="0" style="width:1000px; text-align: left">
    <tbody>
        <tr>
            <td style="width: 10%">
                Исполнитель:
            </td>
            <td>
                {{act_executor}}
            </td>
        </tr>
        <tr>
            <td>
                Заказчик:
            </td>
            <td>
                {{act_customer}}
            </td>
        </tr>
        <tr>
            <td>
                По счету:
            </td>
            <td>
                {{act_on_account}}
            </td>
        </tr>
    </tbody>
</table>
            
            <table border="1" cellpadding="0" cellspacing="0" style="width:1000px">
    <tbody>
        <tr style="text-align: center">
            <td>
                №
            </td>
            <td>
                <strong>Наименование работ, услуг</strong>
            </td>
            <td>
                <strong>Кол-во</strong>
            </td>
            <td>
                <strong>Ед.</strong>
            </td>
            <td>
                <strong>Цена</strong>
            </td>
            <td>
                <strong>Сумма</strong>
            </td>
        </tr>
        <!--tr_part--><tr><td><!--tr_part-->{{tr_part}}<!--tr_part--></td></tr><!--tr_part-->
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" style="width:1000px; text-align: right">
    <tbody>
        <tr>
            <td style="width: 80%">
                <strong>Итого:</strong>
            </td>
            <td>
                <strong>{{act_total_amount}}</strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>В том числе НДС:</strong>
            </td>
            <td>
                <strong>{{nds_total}}</strong>
            </td>
        </tr>
    </tbody>
</table>
<div>Всего оказано услуг {{act_total_count}}, на сумму {{act_total_summ}} руб.</div>
<div><strong>{{act_total_summ_text}}</strong></div>
<div>&nbsp;</div>
<div>Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам
оказания услуг не имеет.
</div>
<div>&nbsp;</div>
<div>Настоящий акт составлен в двух экземплярах, по одному для каждой стороны.</div>
<div>&nbsp;</div>
<div>Заказчик должен в течение 14 (четырнадцати) дней направить в адрес Исполнителя подписанный со своей стороны
Акт, либо письменный мотивированный отказ от подписания Акта. В случае если по истечении указанного выше
срока от Заказчика не будет получен письменный мотивированный отказ от подписания Акта, обязательства
Исполнителя считаются выполненными.</div>
<div style="border-bottom: 2px solid #000; width: 1000px; padding-bottom: 20px;"></div>
<div>&nbsp;</div>
<table border="0" cellpadding="0" cellspacing="0" style="width:1000px; text-align: left">
    <tbody>
        <tr>
            <td style="width: 50%">
                <strong>Исполнитель</strong>
            </td>
            <td style="padding-left : 20px;">
            </td>
            <td style="width: 50%">
                <strong>Заказчик</strong>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid #000; padding-bottom: 30px;">
                 {{act_executor_executive_position}}, {{act_executor_organization}}
            </td>
            <td>
            </td>
            <td style="border-bottom: 1px solid #000; padding-bottom: 30px;">
                {{act_customer_executive_position}}, {{act_customer_organization}}
            </td>
        </tr>
        <tr>
            <td>
                {{act_executor_director_organization}}
            </td>
            <td>
            </td>
            <td>
                {{act_customer_director_organization}}
            </td>
        </tr>
    </tbody>
</table>