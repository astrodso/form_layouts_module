<div style="font-family: sans-serif;">
    <div style="text-align: right; font-size: 7px">
        <div>Приложение No 1 к постановлению Правительства Российской Федерации от 26 декабря 2011 г. No 1137</div>
        <div>(в редакции постановления Правительства Российской Федерации от 19 августа 2017 г. No 981)</div>
    </div>
    <div style="width: 900px;">
        <div><strong style="font-size: 15px">Счет-фактура № {{invoice_number}} от {{invoice_date}} г.</strong></div>
        <div><strong style="font-size: 15px">Исправление № -- от --</strong></div>
    </div>
    <div style="font-size: 10px;">
        <div>Продавец: {{invoice_organization}}</div>
        <div>Адрес: {{invoice_ur_address_organization}}</div>
        <div>ИНН/КПП продавца: {{invoice_inn_seller}}/{{invoice_kpp_seller}}</div>
        <div>Грузоотправитель и его адрес: --</div>
        <div>Грузополучатель и его адрес:--</div>
        <div>К платежно-расчетному документу {{invoice_payments}}</div>
        <div>Покупатель: {{invoice_buyer}}</div>
        <div>Адрес: {{invoice_ur_address_buyer}}</div>
        <div>ИНН/КПП покупателя: {{invoice_inn_buyer}}/{{invoice_kpp_buyer}}</div>
        <div>Валюта: наименование, код Российский рубль, 643</div>
        <div>Идентификатор государственного контракта, договора (соглашения) (при наличии):</div>
    </div>
    <div>&nbsp;</div>
    <table border="1" cellpadding="0" cellspacing="0" style="width: 1000px; font-size: 10px; text-align: center; vertical-align: middle;">
        <tr>
            <td colspan="1" rowspan="2" style="width: 25%">
                Наименование товара (описание выполненных работ, оказанных услуг), имущественного права<br>
                &nbsp;</td>
            <td colspan="1" rowspan="2" style="width: 5%">
                Код вида товара</td>
            <td colspan="2" rowspan="1" style="width: 10%">
                Единица<br>
                измерения</td>
            <td colspan="1" rowspan="2">Коли-
                чество
                (объем)</td>
            <td colspan="1" rowspan="2">Цена (тариф)
                за единицу
                измерения</td>
            <td colspan="1" rowspan="2">Стоимость
                товаров (работ,
                услуг),
                имущественных
                прав без налога -
                всего</td>
            <td colspan="1" rowspan="2">В том
                числе
                сумма
                акциза</td>
            <td colspan="1" rowspan="2">Налоговая
                ставка</td>
            <td colspan="1" rowspan="2">Сумма налога,
                предъявляемая
                покупателю</td>
            <td colspan="1" rowspan="2">Стоимость товаров
                (работ, услуг),
                имущественных
                прав с налогом -
                всего</td>
            <td colspan="2" rowspan="1">Страна
                происхождения товара</td>
            <td colspan="1" rowspan="2">Регистрационный номер
                таможенной
                декларации</td>
        </tr>
        <tr>
            <td style="width: 5%">код</td>
            <td style="width: 10%">
                условное обозначение (национальное)</td>
            <td>цифровой
                код</td>
            <td>краткое
                наименование</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1a</td>
            <td>2</td>
            <td>2a</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>10a</td>
            <td>11</td>
        </tr>
        <!--tr_part--><tr><td><!--tr_part-->{{tr_part}}<!--tr_part--></td></tr><!--tr_part-->
        <tr>
            <td colspan="6">Всего к оплате</td>
            <td>{{invoice_complete_summ_without_nds}}</td>
            <td colspan="2">X</td>
            <td>{{invoice_complete_nds}}</td>
            <td>{{invoice_complete_summ}}</td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <table border="0" cellpadding="0" cellspacing="10" style="width: 800px; font-size: 10px; text-align: left;">
        <tbody>
            <tr>
                <td style="width: 20%">Руководитель организации или иное уполномоченное лицо</td>
                <td></td>
                <td>{{invoice_organization_director}}</td>
                <td style="width: 20%">Главный бухгалтер или иное уполномоченное лицо</td>
                <td></td>
                <td>{{invoice_organization_main_accountant}}</td>
            </tr>
            <tr>
                <td></td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(подпись)</td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(ф.и.о.)</td>
                <td>
                    &nbsp;</td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(подпись)</td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(ф.и.о.)</td>
            </tr>
            <tr>
                <td>Индивидуальный предприниматель или иное уполномоченное лицо</td>
                <td></td>
                <td></td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td></td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(подпись)</td>
                <td style="width: 10%; border-top: solid 1px #000; text-align: center">(ф.и.о.)</td>
                <td colspan="3" style="border-top: solid 1px #000; text-align: center">(реквизиты свидетельства о государственной регистрации индивидуального предпринимателя)										</td>
            </tr>
        </tbody>
    </table>

</div>

