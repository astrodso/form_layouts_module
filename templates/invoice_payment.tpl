<div><strong>{{organization}}</strong></div>

<div><strong>{{organization_address}}</strong></div>

<div><strong>Телефон: {{office_phone}}</strong></div>

<div>&nbsp;</div>
<div>&nbsp;</div>

<table border="1" cellpadding="0" cellspacing="0" style="width:1000px">
    <tbody>
        <tr>
            <td colspan="2" rowspan="2">
                {{bank_organization}}
                <br>Банк получателя
            </td>
            <td>
                БИК
            </td>
            <td rowspan="2">
                {{bik_bank_organization}}<br>
                {{corr_bank_organization}}
            </td>
        </tr>
        <tr>
            <td>
                Сч. №
            </td>
        </tr>
        <tr>
            <td>
                ИНН {{inn_organization}}
            </td>
            <td>
                КПП {{kpp_organization}}
            </td>
            <td rowspan="2">
                Сч. №
            </td>
            <td rowspan="2">
                {{bank_account_organization}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                {{organization}}
                <div>&nbsp;</div>
                Получатель
            </td>
        </tr>
    </tbody>
</table>

<h1 style="border-bottom: 2px solid #000; width: 1000px; padding-bottom: 20px;"><strong>Счет на оплату {{account_number}} от {{account_date}} г.</strong></h1>

<div>
    Поставщик (исполнитель): <strong>{{organization}}, ИНН {{inn_organization}}, КПП {{kpp_organization}}, {{organization_address}}</strong>
</div>
<div>
    Покупатель (заказчик): <strong>{{payer}}, ИНН {{inn_payer}}, КПП {{kpp_payer}}, {{payer_address}}</strong>
</div>
<br>
<div>
    Основание: <strong>{{contract_name}}</strong>
</div>
<div><br></div>

<table border="1" cellpadding="0" cellspacing="0" style="width:1000px">
    <tbody>
        <tr style="text-align: center">
            <td>
                №
            </td>
            <td>
                <strong>Товар (Услуга)</strong>
            </td>
            <td>
                <strong>Кол-во</strong>
            </td>
            <td>
                <strong>Ед.</strong>
            </td>
            <td>
                <strong>Цена</strong>
            </td>
            <td>
                <strong>Сумма</strong>
            </td>
        </tr>
        <!--tr_part--><tr><td><!--tr_part-->{{tr_part}}<!--tr_part--></td></tr><!--tr_part-->
    </tbody>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:1000px; text-align: right">
    <tbody>
        <tr>
            <td style="width: 70%">
                <strong>Итого:</strong>
            </td>
            <td>
                <strong>{{service_total_summ}}</strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>{{service_nds_not_nds_desc}}</strong>
            </td>
            <td>
                <strong>{{service_nds_not_nds_val}}</strong>
            </td>
        </tr>
        <tr>
            <td>
                <strong>Всего к оплате:</strong>
            </td>
            <td>
                <strong>{{service_total_summ}}</strong>
            </td>
        </tr>
    </tbody>
</table>
<div>Всего наименований {{service_total_count}}, на сумму {{service_total_summ}} руб.</div>
<div><strong>{{service_total_summ_text}}</strong></div>
<br>
<br>
<div>Убедительная просьба, при оплате в графе "Назначение платежа" указать следующую информацию:</div>
<div><strong>Оплата по счёту {{account_number}} от {{account_date}} г. за услуги по сертификации.</strong></div>																																											
<br>
<div>Обращаем Ваше внимание на то, что счёт может быть оплачен только фирмой, на которую он был выставлен.</div>
<div><strong>Оплата за третьих лиц запрещена!</strong></div>
<div style="border-bottom: 2px solid #000; width: 1000px; padding-bottom: 10px;">Оплата данного счета означает согласие с условиями Договора.</div>
<br>
<table border="0" cellpadding="0" cellspacing="0" style="width:500px;">
    <tr>
        <td>
            <strong>Руководитель</strong>
        </td>
        <td>
            <span style="border-bottom: 1px solid #000; width: 200px; padding-bottom: 1px; padding-left:100px;">
                {{organization_director}}
            </span>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Бухгалтер</strong>
        </td>
        <td>
            <span style="border-bottom: 1px solid #000; width: 200px; padding-bottom: 1px; padding-left:100px;">
                {{organization_main_accountant}}
            </span>
        </td>
    </tr>
</table>
