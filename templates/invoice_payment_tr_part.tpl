<tr>
    <td style="text-align: center">
        {{service_number}}
    </td>
    <td>
        {{service}}
    </td>
    <td style="text-align: center"> 
        {{service_count}}
    </td>
    <td style="text-align: right">
        {{service_unit}}
    </td>
    <td style="text-align: right">
        {{service_price}}
    </td>
    <td style="text-align: right">
        {{service_summ}}
    </td>
</tr>