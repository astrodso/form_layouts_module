<?php

use common\models\enum\EmailEventEnum;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\module\form_layouts\models\enum\HtmlTemplatesEnum;
use \backend\module\form_layouts\models\enum\FormLayoutsStatusEnum;
use backend\module\form_layouts\models\enum\FormLayoutsEntityTypeEnum;

/* @var $this yii\web\View */
/* @var $model common\models\EmailtemplateModel */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="emailtemplate-model-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput();?>

    <?= $form->field($model, 'entityType')->dropDownList(FormLayoutsEntityTypeEnum::listData());?>

    <?= $form->field($model, 'layoutType')->dropDownList(HtmlTemplatesEnum::listData(), [
        'onchange' =>
        '$.post("'.Yii::$app->urlManager->createUrl(["form_layouts/default/get-layout"]).'",
            {
                layoutType: $(this).val()
            },
            function( data ) {
                $(".cke_wysiwyg_frame").contents().find("body").html(data)
            }
        )',
        'prompt' => 'Выбрать...'
    ]);?>

    <?= $form->field($model, 'status')->dropDownList(FormLayoutsStatusEnum::listData());?>

    <?= $form->field($model, 'orientation')->checkbox();?>

    <?= $form->field($model, 'body')->widget(CKEditor::class, [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => ['allowedContent' => true],
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Редактировать'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
