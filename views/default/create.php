<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EmailtemplateModel */

$this->title = Yii::t('app', 'Создание макета формы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Макеты печатных форм'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"><?= Html::encode($this->title) ?></h3></div>
            <div class="panel-body">

    <?= $this->render('_form', [
        'model' => $model,
       
    ]) ?>
            </div> <!-- panel-body -->
        </div> <!-- panel -->
    </div> <!-- col -->
</div> <!-- End row -->
