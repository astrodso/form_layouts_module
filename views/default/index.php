<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\BaseStringHelper;
use backend\module\form_layouts\models\enum\FormLayoutsEntityTypeEnum;
use backend\module\form_layouts\models\enum\HtmlTemplatesEnum;
/* @var $this yii\web\View */
/* @var $searchModel backend\module\form_layouts\models\HtmlTemplatesSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Макеты печатных форм';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="html-templates-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'body',
                'value' => function($model){
                    return BaseStringHelper::truncate( $model->body, 50 );
                }
            ],
            [
                'attribute' => 'layoutType',
                'value' => function($model){
                    return isset( HtmlTemplatesEnum::$list[$model->layoutType]) ? HtmlTemplatesEnum::$list[$model->layoutType] : '';
                }
            ],
            [
                'attribute' => 'entityType',
                'value' => function($model){
                    return isset( FormLayoutsEntityTypeEnum::$list[$model->entityType]) ? FormLayoutsEntityTypeEnum::$list[$model->entityType] : '';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
