<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\module\form_layouts\models\HtmlTemplates */

$this->title = 'Редактирование макета формы: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Макеты печатных форм', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="html-templates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
