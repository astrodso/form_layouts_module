<?php

namespace backend\module\form_layouts\widgets;

use yii\base\Widget;

class ButtonWidget extends Widget
{
    public $dataTargetId;

    public function run()
    {
        return $this->render('button', [
            'dataTargetId' => $this->dataTargetId,
        ]);
    }
}