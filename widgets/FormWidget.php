<?php

namespace backend\module\form_layouts\widgets;

use Yii;
use yii\base\Widget;
use backend\module\form_layouts\models\FormLayouts;

class FormWidget extends Widget
{
    public $entityId;
    public $entityType;
    public $contanerId;

    public function run()
    {

        $model = FormLayouts::find()->where(['entityType' => $this->entityType])->all();

        return $this->render('form', [
            'model' => $model,
            'entityType' => $this->entityType,
            'entityId' => $this->entityId,
            'contanerId' => $this->contanerId
        ]);
    }
}