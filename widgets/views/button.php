<?php

use yii\bootstrap\Modal;
?>
<?= Modal::widget([
    'toggleButton' => [
        'tag'   => 'button',
        'data-target' => '#'.$dataTargetId,
        'class' => 'btn btn-primary',
        'label' => 'Печать',
    ],
    'clientOptions' => false,
    'header' => null
]);
?>