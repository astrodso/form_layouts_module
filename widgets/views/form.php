<?php

use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use backend\module\form_layouts\models\enum\HtmlTemplatesEnum;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
?>
<?php
Modal::begin([
    'header'       => '<h2>Печать</h2>',
    'size'         => Modal::SIZE_LARGE,
    'toggleButton' => false,
    'id' => $contanerId,
]);
?>
<?=
Html::dropDownList('list', null, ArrayHelper::map($model, 'id', 'name'),
    [
    'prompt'   => 'Выбрать...',
    'class'    => 'form-control',
    'onchange' =>
    '$.post("'.Yii::$app->urlManager->createUrl(["form_layouts/default/get-form"]).'",
        {
            formId: $(this).val(),
            entityType: "'.$entityType.'",
            entityId: "'.$entityId.'"
        },
        function( data ) {
            $(".cke_wysiwyg_frame").contents().find("body").html(data)
        }
    )',
])
?>
<div>&nbsp;</div>
<?=
CKEditor::widget([
    'name'    => 'test',
    'id'      => 'w'.mt_rand(2, 100),
    'options' => [
        'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
    ]
]);
?>

<?= Html::button('Добавить печать', ['class' => 'btn-primary']) ?>
<div>&nbsp;</div>

<?php Modal::end(); ?>